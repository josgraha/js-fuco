import R from 'ramda'
import fs from 'fs'
import { IO } from '../io'
const { equals, first, isEmpty, second, } = R
const { log } = console

describe('GIVEN an io monad', () => {
  describe ('WHEN some input values are given', () => {
    const getData = () => {
      let dataList
      if (!dataList) {
        dataList = fs.readFileSync('./io.test.txt').split('\n')
      }
      console.log(`dtaList: count: ${dataList.length}, items: `, dataList)
      return dataList
    }
    const getUserName = () => getData().first(dataList)
    const getPassword = () => getData().second(dataList)
    const checkPassword = ({ username, pass }) => {
      const isCorrect = pass === '12345'
      return isCorrect ?
          `Welcome ${ username }, to the machine` :
          `Whoa ${ username }! Come back correct!`
      return isCorrect
    }

    it('THEN should have the correct IO for datafile', () => {
      const io = IO.of(getUser)
        .chain(askForPassword)
        .ap(IO.of(() => checkPassword))
        .run()
      console.log(io)
      expect(true).toBe(true)
    })

  })
})
