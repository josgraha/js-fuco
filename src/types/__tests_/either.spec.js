import R from 'ramda'
import { Right, Left, Either } from '../either'
const { equals } = R
const { log } = console

describe('GIVEN an either monad', () => {
    describe('WHEN an either is chained', () => {
        it('THEN right should be last value', () => {
            const right = Right.of(100).chain(x => Right.of(x * 100))
            const chainedValue = 100 * 100
            expect(
                right.value
            ).toBe(chainedValue)
        })
        it('THEN should give left on error', () => {
            const errorString = 'That is not JSON Jason!'
            const result = Either.encase(errorString, () => JSON.parse('{ a: 10 }'))
            expect(
                result.value
            ).toBe(errorString)
        })
    })
})
