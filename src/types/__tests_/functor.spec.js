import R from 'ramda'
import { compose } from '../../core'
import Functor from '../functor.js'
const { equals, map } = R
const { log } = console

describe('GIVEN a functor', () => {
    const id = x => x
    const f = price => (price * 1.07) + 10
    const g = total => 100 - total
    const fg = compose(g, f)
    const fa = Functor.of(90)
    
    describe('WHEN passed the identity function', () => {
        it('THEN it is identical to another functor with the same value', () => {
            expect(
                equals(fa.fmap(id), Functor.of(id(90)))
            ).toBe(true)
        })
        it('THEN it is identical to a functor with same value', () => {
            expect(
                equals(fa.fmap(id), fa)
            ).toBe(true)         
        })
        it('THEN composition should be identical chained', () => {
            expect(
                equals(
                    fa.fmap(f).fmap(g),
                    Functor.of(fg(90))
                )
            ).toBe(true)
        })
    })
})
