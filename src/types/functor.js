const { log } = console

class Functor {

  static of (value) {
    return new Functor(value)
  }
  
  constructor (value) {
    this.value = value
  }

  fmap (fn) {
    return new Functor(fn(this.value))
  }

}

Functor[ 'fantasy-land/of' ] = Functor.of
Functor.prototype[ 'fantasy-land/map' ] = Functor.prototype.fmap

export default Functor
