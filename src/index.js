export { default as Functor } from './functor'
export {
  Maybe,
  Just,
  Nothing,
  isNothing,
  isJust,
  getOrElse,
} from './types/maybe'
export {
  Right,
  Left,
  of as ofEither,
  toEither,
  encase
} from './types/either'
