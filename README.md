# JavaScript (JS) _Fu_nctional _Co_ncepts

## Description
*Note* this is a *Work In Progress* (WIP)
Functional concepts implemented in JS.  In an effort to gain a better understanding of Functional Programming concepts it seemed useful to gather code snippets from various online courses and books.


Includes the following Monad types.
* Functor
* Maybe
* Either
* IO

Typeclass specifcations taken from the [FantasyLand Spec](https://github.com/fantasyland/fantasy-land)
